package com.robert.testebuilders.cliente.repository;

import com.robert.testebuilders.cliente.model.Cliente;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

@Repository
public interface IClienteRepository
                extends PagingAndSortingRepository<Cliente, String>, JpaSpecificationExecutor<Cliente> {

}
