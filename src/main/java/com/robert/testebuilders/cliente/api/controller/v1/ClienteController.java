package com.robert.testebuilders.cliente.api.controller.v1;

import javax.validation.Valid;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.robert.testebuilders.cliente.model.Cliente;
import com.robert.testebuilders.cliente.repository.IClienteRepository;

import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import net.kaczmarzyk.spring.data.jpa.domain.LikeIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.web.annotation.*;

@RestController
@RequestMapping("api/v1/cliente")
public class ClienteController {

    private final IClienteRepository clienteRepository;
    private ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
            false);

    public ClienteController(IClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    @GetMapping
    public Page<Cliente> findAll(@Or({ @Spec(path = "nome", params = "nome", spec = LikeIgnoreCase.class),
            @Spec(path = "cpf", params = "cpf", spec = LikeIgnoreCase.class) }) Specification<Cliente> customerSpec,
            Pageable pageable) {

        return clienteRepository.findAll(customerSpec, pageable);
    }

    @PostMapping
    public Cliente save(@RequestBody @Valid Cliente entity) {
        return clienteRepository.save(entity);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable("id") String id, @RequestBody @Valid Cliente entity) {
        entity.setId(id);
        clienteRepository.save(entity);
        return ResponseEntity.ok("resource updated");
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable("id") String id) {
        clienteRepository.deleteById(id);
        return ResponseEntity.ok("resource deleted");
    }

    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    public ResponseEntity<Cliente> updateCliente(@PathVariable String id, @RequestBody JsonPatch patch)
            throws JsonProcessingException, JsonPatchException {

        try {

            Cliente cliente = clienteRepository.findById(id).orElseThrow(NotFoundException::new);
            Cliente clientePatched = applyPatchToCliente(patch, cliente);

            clienteRepository.save(clientePatched);
            return ResponseEntity.ok(cliente);

        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (JsonPatchException | JsonProcessingException e) {
            System.out.println(e.getMessage() + e.getStackTrace());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    private Cliente applyPatchToCliente(JsonPatch patch, Cliente targetCustomer)
            throws JsonPatchException, JsonProcessingException {
        JsonNode patched = patch.apply(objectMapper.convertValue(targetCustomer, JsonNode.class));
        return objectMapper.treeToValue(patched, Cliente.class);
    }

}
