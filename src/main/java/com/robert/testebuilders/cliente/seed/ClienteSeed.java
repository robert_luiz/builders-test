package com.robert.testebuilders.cliente.seed;

import java.sql.Date;
import java.util.ArrayList;

import javax.annotation.PostConstruct;

import com.robert.testebuilders.cliente.model.Cliente;
import com.robert.testebuilders.cliente.repository.IClienteRepository;

import org.springframework.stereotype.Service;

@Service
public class ClienteSeed {

    private final IClienteRepository clienteRepository;

    public ClienteSeed(IClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    @PostConstruct
    public void SeedClient() {
        var listaClientes = new ArrayList<Cliente>();
        listaClientes.add(new Cliente("4e428f82-61d2-11eb-ab01-afca473c09bd", "Robert Luiz", "51660383048",
                Date.valueOf("1987-05-23")));
        listaClientes.add(new Cliente("42348fe1-4470-41be-92e3-3bec5d7791d8", "João Algunsto", "96825243004",
                Date.valueOf("1966-12-03")));
        listaClientes.add(new Cliente("9a5cbd3c-c338-4c6f-afd4-c46bff21d7ab", "Alberto Leandro", "79644895002",
                Date.valueOf("1997-08-15")));

        clienteRepository.saveAll(listaClientes);
    }

}
