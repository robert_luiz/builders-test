package com.robert.testebuilders.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

public class DataUtil {

    public static Integer diferencaEmDias(Date data1, Date data2) {
        return diferencaEmDias(DataUtil.toLocalDateTime(data1), DataUtil.toLocalDateTime(data2));
    }

    public static Integer diferencaEmMeses(Date data1, Date data2) {
        return diferencaEmMeses(DataUtil.toLocalDateTime(data1), DataUtil.toLocalDateTime(data2));
    }

    public static Integer diferencaEmSemanas(Date data1, Date data2) {
        return diferencaEmSemanas(DataUtil.toLocalDateTime(data1), DataUtil.toLocalDateTime(data2));
    }

    public static Integer diferencaEmDias(LocalDateTime localDateTime1, LocalDateTime localDateTime2) {
        return diferencaEm(ChronoUnit.DAYS, localDateTime1, localDateTime2);
    }

    public static Integer diferencaEmSemanas(LocalDateTime localDateTime1, LocalDateTime localDateTime2) {
        return diferencaEm(ChronoUnit.WEEKS, localDateTime1, localDateTime2);
    }

    public static Integer diferencaEmMeses(LocalDateTime localDateTime1, LocalDateTime localDateTime2) {
        return diferencaEm(ChronoUnit.MONTHS, localDateTime1, localDateTime2);
    }

    private static Integer diferencaEm(ChronoUnit unidade, LocalDateTime localDateTime1, LocalDateTime localDateTime2) {
        return Math.abs(Math.toIntExact(unidade.between(localDateTime1, localDateTime2)));
    }

    public static LocalDateTime toLocalDateTime(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static LocalDate toLocalDate(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static Date firstHourOfDay(Date date) {
        Date dateReturn = DateUtils.setHours(date, 0);
        dateReturn = DateUtils.setMinutes(dateReturn, 0);
        dateReturn = DateUtils.setSeconds(dateReturn, 0);
        dateReturn = DateUtils.setMilliseconds(dateReturn, 0);

        return dateReturn;
    }

    public static Date lastHourOfDay(Date date) {
        Date dateReturn = DateUtils.setHours(date, 23);
        dateReturn = DateUtils.setMinutes(dateReturn, 59);
        dateReturn = DateUtils.setSeconds(dateReturn, 59);
        dateReturn = DateUtils.setMilliseconds(dateReturn, 9);

        return dateReturn;
    }

}
